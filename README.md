# PclSharp 1.12.0 依赖的 VTK 9.0 库 DLL 文件

## 描述

本仓库提供了一个资源文件，包含了 PclSharp 1.12.0 版本依赖的 VTK 9.0 库的 DLL 文件。这些 DLL 文件是解决在使用 PclSharp 时遇到的 `PclSharp.Extern.dll` 无法加载的问题所必需的。

## 使用方法

1. 下载本仓库中的 `vtk9.0` 文件夹。
2. 将 `vtk9.0` 文件夹下的所有 DLL 文件拷贝到你的工程的 `debug` 或 `release` 目录下。
3. 重新编译并运行你的项目，即可解决 `PclSharp.Extern.dll` 无法加载的报错问题。

## 注意事项

- 请确保你使用的是 PclSharp 1.12.0 版本，并且依赖的 VTK 版本为 9.0。
- 如果你使用的是其他版本的 PclSharp 或 VTK，可能需要不同的 DLL 文件。

## 贡献

如果你在使用过程中遇到任何问题或有任何改进建议，欢迎提交 Issue 或 Pull Request。

## 许可证

本仓库中的文件遵循相应的开源许可证。具体请查看每个文件的许可证信息。